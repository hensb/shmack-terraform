FROM alpine:latest
MAINTAINER hendrik.schoeneberg@zuehlke.com

# add shmack user
ARG SHMACK_HOME=/home/shmack/
RUN addgroup shmack && adduser -D -h "${SHMACK_HOME}" -G shmack shmack

# add terraform bootstrap folder to user's home
COPY terraform/ "${SHMACK_HOME}"

# add terraform
RUN mkdir -p /opt/terraform/ &&\
    wget https://releases.hashicorp.com/terraform/0.11.1/terraform_0.11.1_linux_amd64.zip -O /tmp/terraform.zip &&\
    unzip /tmp/terraform.zip -d /opt/terraform &&\
    rm -f /tmp/terraform.zip &&\
    chown -R shmack:shmack /opt/terraform "${SHMACK_HOME}"

ENV PATH="/opt/terraform:${PATH}"

# add openssh
RUN apk --update add openssh &&\
    rm -rf /var/cache/apk/*

USER shmack
CMD ["/bin/sh"]