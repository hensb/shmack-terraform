# SHMACK Terraform

This is a SHMACK docker image which automates the following steps

* Creating an AWS admin user
* Creating an AWS key-pair
* Bootstrapping a Mesosphere installation using a CloudFormation template
* Installing (S)park, (C)assandra and (K)afka on (M)esosphere
* Providing an example Java project with a Gradle task to submit Spark Jobs using the DC/OS CLI

## Running the Container

> docker pull hensb/shmack-terraform  
> docker run -it -d --name shmack -e TF_VAR_aws_access_key=<my_aws_key> -e TF_VAR_aws_secret_key=<my_secret_aws_key> hensb/shmack-terraform

That's it!
