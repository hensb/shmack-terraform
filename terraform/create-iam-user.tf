provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region = "us-west-1"
}

resource "aws_iam_user" "shmack" {
  name = "shmack-user"
  force_destroy = "true"
}

resource "aws_iam_access_key" "shmack" {
  user = "${aws_iam_user.shmack.name}"
}

resource "aws_key_pair" "shmack-key-pair" {
  key_name   = "shmack-key-pair"
  public_key = "${file("shmackKey.pub")}"

  depends_on = ["null_resource.create-ssh-key"]
}

resource "null_resource" "create-ssh-key" {
  provisioner "local-exec" {
    command = "ssh-keygen -t rsa -b 4096 -N '' -f shmackKey"
    interpreter = ["/bin/sh", "-c"]
  }
}

resource "aws_iam_user_policy" "admin" {
  name = "admin"
  user = "${aws_iam_user.shmack.name}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "*",
            "Resource": "*"
        }
    ]
}
EOF
}