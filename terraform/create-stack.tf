resource "aws_cloudformation_stack" "shmack-stack" {
  name = "shmack-stack"
  capabilities = ["CAPABILITY_IAM"]

  parameters {
    KeyName = "${aws_key_pair.shmack-key-pair.key_name}",
  }

  template_url = "${var.aws_cloudformation_template_url}"
}