resource "aws_s3_bucket_object" "shmack-cf-template" {
  bucket = "shmack-cf-template"
  key    = "template.json"
  source = "cf-templates/single-master.spot.1.10.json"
  etag   = "${md5(file("cf-templates/single-master.spot.1.10.json"))}"
}