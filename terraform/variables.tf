variable "aws_access_key" {
  type = "string"
}

variable "aws_secret_key" {
  type = "string"
}

variable "aws_cloudformation_template_url" {
  type = "string"
  default = "https://s3.eu-central-1.amazonaws.com/shmack-cf-templates/single-master.cloudformation.spot.1.10.json"
}